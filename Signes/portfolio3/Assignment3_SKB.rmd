---
title: "Assignment3"
author: "RF"
date: "3/6/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
setwd("~/Dropbox/AU/4_semester/Computational modeling for cognitive science/R/THK_portfolios/Signes/portfolio3")

data <- read.csv("Assignment3Data.csv")

library(pacman)
p_load(lme4,rethinking,brms,tidyverse)

```


## Multivariate linear models

In this assignment you will investigate the relation between different aspects of IQ and symptom severity in ASD (as measured by ADOS). The assignment is meant to make you practice on linear models, multiple predictors and interactions.

The data you will use is the outcome of the psychological testing of the children you analyzed in methods 3.
Data: https://www.dropbox.com/s/hom2qnmp9hyfwmu/Assignment3Data.csv?dl=0
The data consists of ChildID, gender, age, diagnosis (ASD), symptom severity (ADOS), several aspects of IQ (NonVerbal/Perceptual,Verbal,Social) and the visit at which the tests were administered. The questions should be answered by focusing on visit 1.


The questions you will have to answer are:

1. Assess the relation between symptom severity and IQ (focus on visit 1 and children with ASD) and report the model, a plot of the model, a couple of lines describing the quality of the model and interpreting the results. 
P.S. Should you scale? 
P.P.S. Don't forget to motivate your priors. 
P.P.P.S. At least one plot for results and a plot for quality of each model (here and in the next questions) would be appreciated.

```{r}
dataV1ASD <- filter(data,Visit == 1 & ASD == 1)
plot(dataV1ASD)

# centre the whole thing
dataV1ASD$ADOSc <- dataV1ASD$ADOS - mean(dataV1ASD$ADOS)
# dataV1ASD$ADOSc2 <- scale(dataV1ASD$ADOS,scale=FALSE)
# dataV1ASD$ADOSc == dataV1ASD$ADOSc2 # its the same
dataV1ASD$NonVerbalIQc <- scale(dataV1ASD$NonVerbalIQ,scale=FALSE)
dataV1ASD$VerbalIQc <- scale(dataV1ASD$VerbalIQ,scale=FALSE)
dataV1ASD$SocialIQc <- scale(dataV1ASD$SocialIQ,scale=FALSE)
```

1.1. Verbal IQ and ADOS
```{r}
plot(dataV1ASD$VerbalIQc,dataV1ASD$ADOSc)
dens(dataV1ASD$VerbalIQc)
mtext("VerbalIQ centered")
dens(dataV1ASD$ADOSc)
mtext("ADOS centered")

#Define the model 
m1_1 <- bf(ADOS ~ VerbalIQc)

# Ask which priors need to be defined 
get_prior(m1_1, data = dataV1ASD, family = gaussian)

# Define the prior 
prior = c(prior(normal(14,10),class="Intercept"), # intercept is average of data, spread is wide = concervative
          prior(normal(0,1),class="b"), # conservative choice: mean = 0 (we do not assume a correlation, SD = they)
          prior(normal(0,10),class="sigma")) 

# uniform er altid et dårligt valg

m1_1_prior = brm(m1_1,family=gaussian,dataV1ASD,prior=prior,sample_prior = "only",chain=1,iter=500)

#checking: prior predictive check
pp_check(m1_1_prior,nsample=100)  # what is the implications of our priors? 

# Repeat until you are satisfied 
# Prior-hacking?

m1.1 <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b * VerbalIQc,
        a ~ dnorm(14, 10),
        b ~ dnorm(0, 1),
        sigma ~ dnorm(0, 10)),
    data = dataV1ASD)

precis(m1.1, corr=T)

# make plot that superimposes the MAP values for mean ADOS over the actual data
plot( ADOS ~ VerbalIQc , data=dataV1ASD)
abline( a=coef(m1.1)["a"] , b=coef(m1.1)["b"] )

# plot with uncertainties 
# define sequences of verbalIQc to compute predictions for (horisontal axis)
VerbalIQc.seq <- seq(from =-10, to =15 , by=.1)
# use link to compute m for each sample from the posterior and for each verbalIQ in verbalIQc
mu <- link(m1.1,data=data.frame(VerbalIQc=VerbalIQc.seq))

#summarise the distribution of mu 
mu.mean <-apply(mu,2,mean)
mu.HPDI <- apply(mu,2,HPDI,prob=0.89)

# simulate ADOS
sim.ADOS <- sim(m1.1, data =list(VerbalIQc=VerbalIQc.seq),n=1e4)
str(sim.ADOS)

# PI for ADOS
ADOS.PI <- apply(sim.ADOS,2,PI,prob=.89)

#plot raw data
plot(ADOS ~ VerbalIQc,dataV1ASD,col=col.alpha(rangi2,0.5))

# draw MAP line
lines(VerbalIQc.seq,mu.mean)

# draw HPDI region for line
shade(mu.HPDI,VerbalIQc.seq)

# draw PI region for simulated ADOS
shade(ADOS.PI,VerbalIQc.seq)
mtext("Model: ADOS ~ VerbalIQ (centered)")

# visualise posterior distribution estimates
plot( precis(m1.1) ) # does not work for some weird reason
```

When moving from one verbalIQ "unit" on the x-axis, the model predicts a -.44 decrease in ADOS with a standard deviation of .07. Thus, the model predicts a negative relationship between the two: when verbalIQ increases, ADOS decreases. 89% of the posterior possibility lies between -.55 and -.32.
The intercept, a, has a mean of 13.85 with a standard deviation of .54. Since we have centered our predictor, verbalIQ, the estimate for the interept is ADOS at an average verbal IQ (mean = 0 since we have centered verbalIQ). Thus, at an average verbal IQ, ADOS is 13.85. 

Assess quality: plot - does it follow the data?
Plot model with raw data -->  Compare model predictions with data LOOK IN BOOK. 

1.2. Non Verbal IQ and ADOS
```{r}
plot(dataV1ASD$NonVerbalIQc,dataV1ASD$ADOS)
dens(dataV1ASD$NonVerbalIQc) # around -15-15
# spread = 13-42 = 29 
mtext("NonverbalIQ centered")
dens(dataV1ASD$ADOS) # around 0-25
# spread = 0-21
mtext("ADOS")

#Define the model 
m1_2 <- bf(ADOS ~ NonVerbalIQc)

# Ask which priors need to be defined 
get_prior(m1_2, data = dataV1ASD, family = gaussian)

# Define the prior 
prior = c(prior(normal(14,10),class="Intercept"), # intercept is average of data, spread is wide = concervative
          prior(normal(0,1),class="b"), # conservative choice: mean = 0 (we do not assume a correlation, SD = they)
          prior(normal(0,10),class="sigma")) 

# uniform er altid et dårligt valg

m1_2_prior = brm(m1_2,family=gaussian,dataV1ASD,prior=prior,sample_prior = "only",chain=1,iter=500)

#checking: prior predictive check
pp_check(m1_2_prior,nsample=100)  # what is the implications of our priors? 


# Repeat until you are satisfied 
# Prior-hacking?

m1.2 <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b * NonVerbalIQc,
        a ~ dnorm(14, 10),
        b ~ dnorm(0, 1),
        sigma ~ dnorm(0, 10)),
    data = dataV1ASD)

precis(m1.2, corr=T)

# make plot that superimposes the MAP values for mean ADOS over the actual data
plot( ADOS ~ NonVerbalIQc , data=dataV1ASD)
abline( a=coef(m1.2)["a"] , b=coef(m1.2)["b"] )

# plot with uncertainties 
# define sequences of verbalIQc to compute predictions for (horisontal axis)
NonVerbalIQc.seq <- seq(from =-15, to =15 , by=.1)
# use link to compute m for each sample from the posterior and for each verbalIQ in verbalIQc
mu <- link(m1.2,data=data.frame(NonVerbalIQc=NonVerbalIQc.seq))

#summarise the distribution of mu 
mu.mean <-apply(mu,2,mean)
mu.HPDI <- apply(mu,2,HPDI,prob=0.89)

# simulate ADOS
sim.ADOS <- sim(m1.2, data =list(NonVerbalIQc=NonVerbalIQc.seq),n=1e4)
str(sim.ADOS)

# PI for ADOS
ADOS.PI <- apply(sim.ADOS,2,PI,prob=.89)

#plot raw data
plot(ADOS ~ NonVerbalIQc,dataV1ASD,col=col.alpha(rangi2,0.5))

# draw MAP line
lines(NonVerbalIQc.seq,mu.mean)

# draw HPDI region for line
shade(mu.HPDI,NonVerbalIQc.seq)

# draw PI region for simulated ADOS
shade(ADOS.PI,NonVerbalIQc.seq)
mtext("Model: ADOS ~ NonVerbalIQ (centered)")

# visualise posterior distribution estimates
plot( precis(m1.2) ) # does not work for some weird reason
```


1.3. Social IQ and ADOS
```{r}
plot(dataV1ASD$SocialIQc,dataV1ASD$ADOS)
dens(dataV1ASD$SocialIQc) # around -20-35
# spread = 61-105 = 44 
mtext("SocialIQc centered")
dens(dataV1ASD$ADOS) # around 0-25
# spread = 0-21
mtext("ADOS")

#Define the model 
m1_3 <- bf(ADOS ~ SocialIQc)

# Ask which priors need to be defined 
get_prior(m1_3, data = dataV1ASD, family = gaussian)

# Define the prior 
prior = c(prior(normal(14,10),class="Intercept"), # intercept is average of data, spread is wide = concervative
          prior(normal(0,.5),class="b"), # conservative choice: mean = 0 (we do not assume a correlation, SD = they)
          prior(normal(0,10),class="sigma")) 

# uniform er altid et dårligt valg

m1_3_prior = brm(m1_3,family=gaussian,dataV1ASD,prior=prior,sample_prior = "only",chain=1,iter=500)

#checking: prior predictive check
pp_check(m1_3_prior,nsample=100)  # what is the implications of our priors? 


# Repeat until you are satisfied 
# Prior-hacking?

m1.3 <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b * SocialIQc,
        a ~ dnorm(14, 10),
        b ~ dnorm(0, .5),
        sigma ~ dnorm(0, 10)),
    data = dataV1ASD)

precis(m1.3, corr=T)

# make plot that superimposes the MAP values for mean ADOS over the actual data
plot( ADOS ~ SocialIQc , data=dataV1ASD)
abline( a=coef(m1.3)["a"] , b=coef(m1.3)["b"] )

# plot with uncertainties 
# define sequences of verbalIQc to compute predictions for (horisontal axis)
SocialIQc.seq <- seq(from =-20, to =35 , by=.1)
# use link to compute m for each sample from the posterior and for each verbalIQ in verbalIQc
mu <- link(m1.3,data=data.frame(SocialIQc=SocialIQc.seq))

#summarise the distribution of mu 
mu.mean <-apply(mu,2,mean)
mu.HPDI <- apply(mu,2,HPDI,prob=0.89)

# simulate ADOS
sim.ADOS <- sim(m1.3, data =list(SocialIQc=SocialIQc.seq),n=1e4)
str(sim.ADOS)

# PI for ADOS
ADOS.PI <- apply(sim.ADOS,2,PI,prob=.89)

#plot raw data
plot(ADOS ~ SocialIQc,dataV1ASD,col=col.alpha(rangi2,0.5))

# draw MAP line
lines(SocialIQc.seq,mu.mean)

# draw HPDI region for line
shade(mu.HPDI,SocialIQc.seq)

# draw PI region for simulated ADOS
shade(ADOS.PI,SocialIQc.seq)
mtext("Model: ADOS ~ SocialIQ (centered)")

# visualise posterior distribution estimates
plot( precis(m1.3) ) # does not work for some weird reason
```

Quality assessment
```{r}
compare(m1.1,m1.2,m1.3)
```


2. Do the different aspects of IQ account for different portions of the variance in ADOS? 
- covariation (with all predictors)
- maybe make model and plot where we keep predictors constant (except one) 

2.1. Does it make sense to have all IQ measures in the same model? First write a few lines answering the question and motivating your answer, including a discussion as to what happens when you put all of them in the same model. Then build a model following your answer. If your answer is "no", you are not free, you still have to answer: are there alternative ways of answering the question?
- multicollinearity (if predictors explain same variance)
- - If they account for the same variance, this does not make sense. 

2.2. Build the model, assess its quality, write a few lines interpreting the results.
```{r}
#Define the model 
m2_2_full <- bf(ADOS ~ VerbalIQc  + NonVerbalIQc + SocialIQc)

# Ask which priors need to be defined 
get_prior(m2_2_full, data = dataV1ASD, family = gaussian)

# Define the prior 
prior = c(prior(normal(14,10),class="Intercept"), # intercept is average of data, spread is wide = concervative
          prior(normal(0,1),class="b",coef="VerbalIQc"), 
          prior(normal(0,1),class="b",coef="NonVerbalIQc"),
          prior(normal(0,.5),class="b",coef="SocialIQc"),
          prior(normal(0,10),class="sigma")) 

m2_2_full_prior = brm(m2_2_full,family=gaussian,dataV1ASD,prior=prior,sample_prior = "only",chain=1,iter=500)

#checking: prior predictive check
pp_check(m2_2_full_prior,nsample=100)  # what is the implications of our priors? 


# Repeat until you are satisfied 
# Prior-hacking?

m2.2.full <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b1 * VerbalIQc + b2*NonVerbalIQ + b3*SocialIQc,
        a ~ dnorm(14, 10),
        b1 ~ dnorm(0, 1),
        b2 ~ dnorm(0, 1),
        b3 ~ dnorm(0, .5),
        sigma ~ dnorm(0, 10)),
    data = dataV1ASD)

precis(m2.2.full, corr=T)

# make plot that superimposes the MAP values for mean ADOS over the actual data
plot( ADOS ~ VerbalIQc + NonVerbalIQc + SocialIQc , data=dataV1ASD)
abline( a=coef(m2.2.full)["a"] , b=c(coef(m2.2.full)["b1"],coef(m2.2.full)["b2"],coef(m2.2.full)["b3"] ))

# visualise posterior distribution estimates
plot( precis(m2.2.full) ) # WORKS TODAY 

# call link without specifying new data
# so it uses original data
mu <- link( m2.2.full )
# summarize samples across cases
mu.mean <- apply( mu , 2 , mean )
mu.PI <- apply( mu , 2 , PI )
# simulate observations
# again no new data, so uses original data
ADOS.sim <- sim( m2.2.full , n=1e4 )
ADOS.PI <- apply( ADOS.sim , 2 , PI )

plot( mu.mean ~ dataV1ASD$ADOS , col=rangi2 , ylim=range(mu.PI) ,
    xlab="Observed ADOS" , ylab="Predicted ADOS" )
abline( a=0 , b=1 , lty=2 )
for ( i in 1:nrow(dataV1ASD) )
    lines( rep(dataV1ASD$ADOS[i],2) , c(mu.PI[1,i],mu.PI[2,i]) ,
        col=rangi2 )

# compute residuals
ADOS.resid <- dataV1ASD$ADOS - mu.mean
# get ordering by ADOS rate
o <- order(ADOS.resid)
# make the plot
dotchart( ADOS.resid[o] , labels=dataV1ASD$Loc[o] , xlim=c(-6,5) , cex=0.6 )
abline( v=0 , col=col.alpha("black",0.2) )
for ( i in 1:nrow(dataV1ASD) ) {
    j <- o[i] # which State in order
    lines( dataV1ASD$ADOS[j]-c(mu.PI[1,j],mu.PI[2,j]) , rep(i,2) )
    points( dataV1ASD$ADOS[j]-c(ADOS.PI[1,j],ADOS.PI[2,j]) , rep(i,2),
            pch=3 , cex=0.6 , col="gray" )
}
mtext("Average prediction error by subject with 89% interval of the mean (black line) and 89% prediction interval (grey +)")



```



3. Let's now include also the TD children. Does it make sense to ask whether IQ and ADOS are related? Motivate your answer. In any case, if you wanted to build a model to answer that question, which model would you build? Run the model, assess its quality, write a few lines interpreting the results.
- Social and verbal intelligence: Maybe a negative correlation 
- Non-verbal intelligence: maybe not as clear - if any?

```{r}
dataV1 <- filter(data,Visit == 1)
plot(dataV1)
# centre the whole thing
dataV1$ADOSc <- dataV1$ADOS - mean(dataV1$ADOS)
# dataV1ASD$ADOSc2 <- scale(dataV1ASD$ADOS,scale=FALSE)
# dataV1ASD$ADOSc == dataV1ASD$ADOSc2 # its the same
dataV1$NonVerbalIQc <- scale(dataV1$NonVerbalIQ,scale=FALSE)
dataV1$VerbalIQc <- scale(dataV1$VerbalIQ,scale=FALSE)
dataV1$SocialIQc <- scale(dataV1$SocialIQ,scale=FALSE)

plot(dataV1)
# omitting NA's by only using complete cases - we only loose two children in the data 
dataV1 <- dataV1[complete.cases(dataV1),]

#Define the model 
m3 <- bf(ADOS ~ VerbalIQc  + NonVerbalIQc + SocialIQc)

# Ask which priors need to be defined 
get_prior(m3, data = dataV1, family = gaussian)

# Define the prior 
prior = c(prior(normal(7.43,10),class="Intercept"), # intercept is average of data, spread is wide = concervative
          prior(normal(0,1),class="b",coef="VerbalIQc"), 
          prior(normal(0,1),class="b",coef="NonVerbalIQc"),
          prior(normal(0,.5),class="b",coef="SocialIQc"),
          prior(normal(0,10),class="sigma")) 

m3_prior = brm(m3,family=gaussian,dataV1,prior=prior,sample_prior = "only",chain=1,iter=500)

#checking: prior predictive check
pp_check(m3_prior,nsample=100)  

# Repeat until you are satisfied 
# Prior-hacking?

m3_0 <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b1 * VerbalIQc + b2*NonVerbalIQ + b3*SocialIQc,
        a ~ dnorm(7.43, 10),
        b1 ~ dnorm(0, 1),
        b2 ~ dnorm(0, 1),
        b3 ~ dnorm(0, .5),
        sigma ~ dnorm(0, 10)),
    data = dataV1)

precis(m3_0, corr=T)

# visualise posterior distribution estimates
plot( precis(m3_0) ) # does not work today

# call link without specifying new data so it uses original data
mu <- link( m3.0 )
# summarize samples across cases
mu.mean <- apply( mu , 2 , mean )
mu.PI <- apply( mu , 2 , PI )
# simulate observations
# again no new data, so uses original data
ADOS.sim <- sim( m3.0 , n=1e4 )
ADOS.PI <- apply( ADOS.sim , 2 , PI )

#colour columns
dataV1$Colour[dataV1$ASD==1]="aquamarine4"
dataV1$Colour[dataV1$ASD==0]="coral"

# add a line to show perfect prediction and lines segments for the confidence intervals
plot( mu.mean ~ dataV1$ADOS , ylim=range(mu.PI) ,
    xlab="Observed ADOS" , ylab="Predicted ADOS", col = dataV1$Colour )
abline( a=0 , b=1 , lty=2 )
for ( i in 1:nrow(dataV1) ){
    lines( rep(dataV1$ADOS[i],2) , c(mu.PI[1,i],mu.PI[2,i]) ,
        col="azure3" )
}
mtext("Predicted ADOS Rate Against Observed ADOS Rate with 89% CI of the Average Prediction")


# compute residuals
ADOS.resid <- dataV1$ADOS - mu.mean
# get ordering by ADOS rate
o <- order(ADOS.resid)
# make the plot
dotchart( ADOS.resid[o] , labels=dataV1$Loc[o] , xlim=c(-6,5) , cex=0.6 )
abline( v=0 , col=col.alpha("black",0.2) )
for ( i in 1:nrow(dataV1) ) {
    j <- o[i] # which State in order
    lines( dataV1$ADOS[j]-c(mu.PI[1,j],mu.PI[2,j]) , rep(i,2) )
    points( dataV1$ADOS[j]-c(ADOS.PI[1,j],ADOS.PI[2,j]) , rep(i,2),
            pch=3 , cex=0.6 , col="gray" )
}
mtext("Average prediction error by subject with 89% interval of the mean (black line) and 89% prediction interval (grey +)")





# Showing there is two distinctive distributions using density plots
ggplot(dataV1, aes(x = VerbalIQc, colour = ASD)) + geom_density(alpha = 0.5)

dataV1$ASDf <- as.factor(dataV1$ASD)

ggplot(dataV1, aes(x=VerbalIQ, fill=ASDf)) + geom_density(alpha=.3) + ggtitle("Density of Verbal IQ (centered) for ASD (blue) and TD (red)") + theme(legend.position = "none")
ggplot(dataV1, aes(x=NonVerbalIQc, fill=ASDf)) + geom_density(alpha=.3) + ggtitle("Density of Nonverbal IQ (centered) for ASD (blue) and TD (red)") + theme(legend.position = "none")
ggplot(dataV1, aes(x=SocialIQc, fill=ASDf)) + geom_density(alpha=.3) + ggtitle("Density of Social IQ (centered) for ASD (blue) and TD (red)") + theme(legend.position = "none")


ggplot(dataV1,aes(x=ADOS,y=VerbalIQ,colour=ASD)) + geom_point(show.legend = FALSE) + geom_smooth(method="lm",aes(colour=ChildID),show.legend
= FALSE,se=FALSE) + facet_wrap(~ASD)

ggplot(dataV1,aes(x=ADOS,y=NonVerbalIQ,colour=ASD)) + geom_point(show.legend = FALSE) + geom_smooth(method="lm",aes(colour=ChildID),show.legend
= FALSE,se=FALSE) + facet_wrap(~ASD)

ggplot(dataV1,aes(x=ADOS,y=SocialIQ,colour=ASD)) + geom_point(show.legend = FALSE) + geom_smooth(method="lm",aes(colour=ChildID),show.legend
= FALSE,se=FALSE) + facet_wrap(~ASD)

```


4. Let's discuss contents:
4.1. You have three scores for IQ, do they show shared variance? Is that the same in TD and ASD? What does that tell us about IQ?
```{r}
dataV1TD <- filter(dataV1, ASD == "0")

m4 <- bf(ADOS ~ VerbalIQc  + NonVerbalIQc + SocialIQc)

# Ask which priors need to be defined 
get_prior(m4, data = dataV1TD, family = gaussian)

# Define the prior 
prior = c(prior(normal(0,10),class="Intercept"), # intercept is average of data, spread is wide = concervative
          prior(normal(0,1),class="b",coef="VerbalIQc"), 
          prior(normal(0,1),class="b",coef="NonVerbalIQc"),
          prior(normal(0,.5),class="b",coef="SocialIQc"),
          prior(normal(0,10),class="sigma")) 

m4 = brm(m4,family=gaussian,dataV1TD,prior=prior,sample_prior = "only",chain=1,iter=500)

#checking: prior predictive check
pp_check(m4,nsample=100)  # what is the implications of our priors? 


# Repeat until you are satisfied 
# Prior-hacking?

m4 <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b1 * VerbalIQc + b2*NonVerbalIQ + b3*SocialIQc,
        a ~ dnorm(0, 10),
        b1 ~ dnorm(0, 1),
        b2 ~ dnorm(0, 1),
        b3 ~ dnorm(0, .5),
        sigma ~ dnorm(0, 10)),
    data = dataV1TD)

precis(m4, corr=T)

p_load(corrplot)
## corrplot 0.84 loaded
M <- cor(dataV1TD[,10:12])
corrplot(M, method = "circle", type = c("lower"))

```


4.2. You have explored the relation between IQ and ADOS. How do you explain that relation from a cognitive perspective? N.B. You can present alternative hypotheses.

5. Bonus questions: Including measurement errors. 
5.1. Let's assume that ADOS has a measurement error of 1. How would you model that in any of the previous models? 
5.2. We know that IQ has an estimated measurement error of 2.12. How would you include that? 





QUESTIONS: 
- What does it mean when we change the priors of the different parameters? We find it difficult to motivate our choices and to understand the consequences of them individually
- Prior-hacking? 
--- Malte clarifies: 
---- If the priors in your prior predictive check look unreasonable, then we have not told the models about our believes properly
---- if we try to make our priors look like the likelihood, we do what the model actually will do - and it does it in a more computationally efficient way (the correct way)

- m1_1: when we use verbalIQ or verbalIQc, the estimates for b and sigma changes. They should stay the same, shouldn't they? NO. They should change 
- How do we access quality of a model in B framework?
- Correlation vs. covariance? 

Centering is a good thing because it makes the intercept mean something else - useful, especially when having multiple predictors

- # visualise posterior distribution estimates
plot( precis(m1.1) ) # does not work for some weird reason




