---
title: "Assignment3"
author: "RF"
date: "3/6/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r loading data, include = FALSE}
library(pacman)
p_load(tidyverse, rethinking, brms)

setwd("C:/Users/Kiri Koppelgaard/Documents/Cognitive Science/4. Semester/Computational thinking/W5 - Generalized Linear Models")

data <- read.csv('Assignment3Data.csv')
```


## Multivariate linear models

In this assignment you will investigate the relation between different aspects of IQ and symptom severity in ASD (as measured by ADOS). The assignment is meant to make you practice on linear models, multiple predictors and interactions.

The data you will use is the outcome of the psychological testing of the children you analyzed in methods 3.
Data: https://www.dropbox.com/s/hom2qnmp9hyfwmu/Assignment3Data.csv?dl=0
The data consists of ChildID, gender, age, diagnosis (ASD), symptom severity (ADOS), several aspects of IQ (NonVerbal/Perceptual,Verbal,Social) and the visit at which the tests were administered. The questions should be answered by focusing on visit 1.

The questions you will have to answer are:

1. Assess the relation between symptom severity and IQ (focus on visit 1 and children with ASD) and report the model, a plot of the model, a couple of lines describing the quality of the model and interpreting the results. P.S. Should you scale? P.P.S. Don't forget to motivate your priors. P.P.P.S. At least one plot for results and a plot for quality of each model (here and in the next questions) would be appreciated.
```{r filtering data, include = FALSE}
#selecting data from visit 1 and diagnosis ASD
dataV1ASD <- filter(data, Visit == 1, ASD == 1)

#centering the variables to make the intercept interpretable
dataV1ASD$NonVerbalIQc <- scale(dataV1ASD$NonVerbalIQ,scale=FALSE)
dataV1ASD$VerbalIQc <- scale(dataV1ASD$VerbalIQ,scale=FALSE)
dataV1ASD$SocialIQc <- scale(dataV1ASD$SocialIQ,scale=FALSE)
```

1.1. Verbal IQ and ADOS
```{r making a model, include = FALSE}
#plotting thw raw data
dens(as.numeric(dataV1ASD$ADOS))
dens(as.numeric(dataV1ASD$VerbalIQc))


#Defining prior
m1_1 <- bf(ADOS ~ VerbalIQc)

# Ask which priors need to be defined
get_prior(m1_1, data = dataV1ASD, family = gaussian)#This is very weak priors, since the distributions cross zero

# Define another prior
prior = c(prior(normal(14, 10), class = "Intercept"),
          prior(normal(0, 1), class = 'b'),#the mean slope, conservative choice
          prior(normal(0, 10), class = 'sigma')) #very vague prior 

# Draw the consequences of priors AND likelihood
m1_1_prior <- brm(m1_1, dataV1ASD, prior = prior, sample_prior = "only")

# Prior Predictive Check
pp_check(m1_1_prior, nsample=100)

```


```{r fitting model, include= FALSE}
#Plotting height and weight against one another to get an idea of how strongly they covary
plot( dataV1ASD$ADOS ~ dataV1ASD$NonVerbalIQc )

#Fitting the model with a predictor 
m1_1 <- map(
            alist(
            ADOS ~ dnorm(mu, sigma) ,
            mu <- a + b*VerbalIQc ,
            a ~ dnorm(14, 10),
            b ~ dnorm(0 , 1) ,
            sigma ~ dnorm(0 , 10)
            ) ,
            data=dataV1ASD )

#Table of estimates
precis( m1_1)

```

A person with a verbal IQ 1 point higher will in average have 0.44 point lower ADOS with a standard deviation of 0.12. 89 % of the posterior probability lies between -0.56 and -0.33. The estimate of alpha indicates that a person with Verbal IQ of 0 should have an ADOS of 13.89. The estimate for sigma informs us of the width of the distribution of heights around the mean. Since we have centered verbal IQ, the intercept also means the expected value of the outcome variable i.e. ADOS, when the predictor is at its average value


```{r plotting the model, echo = FALSE}
# plot raw data
plot( ADOS ~ VerbalIQc , dataV1ASD , col=col.alpha(rangi2,0.5) )

# define sequence of VerbalIQ to compute predictions for
# these values will be on the horizontal axis
VerbalIQc.seq <- seq( from=-10, to=20 , by=1 )
# use link to compute mu
# for each sample from posterior
# and for each weight in weight.seq
mu <- link( m1_1 , data=data.frame(VerbalIQc=VerbalIQc.seq) )
str(mu)

# summarize the distribution of mu
mu.mean <- apply( mu , 2 , mean )
mu.HPDI <- apply( mu , 2 , HPDI , prob=0.89 )

# draw MAP line
lines(VerbalIQc.seq , mu.mean )
# draw HPDI region for line
shade( mu.HPDI , VerbalIQc.seq)

#prediction intervals

#For every unique weight value, you sample from a gaussian distribution with the correct mean mu for that weight using the correct value of sigma sampled from the posterior distribution. This gives you a collection of simulated heights that embody the uncertainty in the posterior as well as the uncertainty in the gaussian likelihood
sim.ADOS <- sim( m1_1 , data=list(VerbalIQc=VerbalIQc.seq) )
str(sim.ADOS)

ADOS.PI <- apply( sim.ADOS, 2 , PI , prob=0.89 )#summarizing the simulated heights

# draw PI region for simulated heights
shade( ADOS.PI , VerbalIQc.seq )
```


1.2. Non Verbal IQ and ADOS
```{r making a model, include = FALSE}
#plotting thw raw data
dens(as.numeric(dataV1ASD$ADOS))
dens(as.numeric(dataV1ASD$NonVerbalIQc))


#Defining prior
m1_2 <- bf(ADOS ~ NonVerbalIQc)

# Ask which priors need to be defined
get_prior(m1_2, data = dataV1ASD, family = gaussian)#This is very weak priors, since the distributions cross zero

# Define another prior
prior = c(prior(normal(14, 10), class = "Intercept"),
          prior(normal(0, 1), class = 'b'),#the mean slope, conservative choice
          prior(normal(0, 10), class = 'sigma')) #very vague prior 

# Draw the consequences of priors AND likelihood
m1_2_prior <- brm(m1_2, dataV1ASD, prior = prior, sample_prior = "only")

# Prior Predictive Check
pp_check(m1_2_prior, nsample=100)

```


```{r fitting model, include= FALSE}
#Plotting height and weight against one another to get an idea of how strongly they covary
plot( dataV1ASD$ADOS ~ dataV1ASD$NonVerbalIQc )

#Fitting the model with a predictor 
m1_2 <- map(
            alist(
            ADOS ~ dnorm(mu, sigma) ,
            mu <- a + b*NonVerbalIQc ,
            a ~ dnorm(14, 11),
            b ~ dnorm(0 , 1) ,
            sigma ~ dnorm(0, 11)
            ) ,
            data=dataV1ASD )

#Table of estimates
precis( m1_2)

```

A person with a nonverbal IQ 1 point higher will in average have 0.49 point lower ADOS with a standard deviation of 0.12. 89 % of the posterior probability lies between -0.68 and -0.30. The estimate of alpha indicates that a person with non verbal IQ of 0 should have an ADOS of 13.85. The estimate for sigma informs us of the width of the distribution of ADOS around the mean. Since we have centered nonverbal IQ, the intercept also means the expected value of the outcome variable i.e. ADOS, when the predictor is at its average value.


```{r plotting the model, echo = FALSE}
# plot raw data
plot( ADOS ~ NonVerbalIQc , dataV1ASD , col=col.alpha(rangi2,0.5) )

# define sequence of VerbalIQ to compute predictions for
# these values will be on the horizontal axis
NonVerbalIQc.seq <- seq( from=-15, to=16 , by=1 )
# use link to compute mu
# for each sample from posterior
# and for each weight in weight.seq
mu <- link( m1_2 , data=data.frame(NonVerbalIQc=NonVerbalIQc.seq) )
str(mu)

# summarize the distribution of mu
mu.mean <- apply( mu , 2 , mean )
mu.HPDI <- apply( mu , 2 , HPDI , prob=0.89 )

# draw MAP line
lines(NonVerbalIQc.seq , mu.mean )
# draw HPDI region for line
shade( mu.HPDI , NonVerbalIQc.seq)

#prediction intervals

#For every unique weight value, you sample from a gaussian distribution with the correct mean mu for that weight using the correct value of sigma sampled from the posterior distribution. This gives you a collection of simulated heights that embody the uncertainty in the posterior as well as the uncertainty in the gaussian likelihood
sim.ADOS <- sim( m1_2 , data=list(NonVerbalIQc=NonVerbalIQc.seq) )
str(sim.ADOS)

ADOS.PI <- apply( sim.ADOS, 2 , PI , prob=0.89 )#summarizing the simulated heights

# draw PI region for simulated heights
shade( ADOS.PI , NonVerbalIQc.seq )
```


1.3. Social IQ and ADOS

```{r making a model, include = FALSE}
#plotting thw raw data
dens(as.numeric(dataV1ASD$ADOS))
dens(as.numeric(dataV1ASD$SocialIQc))


#Defining prior
m1_3 <- bf(ADOS ~ SocialIQc)

# Ask which priors need to be defined
get_prior(m1_3, data = dataV1ASD, family = gaussian)#This is very weak priors, since the distributions cross zero

# Define another prior
prior = c(prior(normal(14, 10), class = "Intercept"),
          prior(normal(0, 0.5), class = 'b'),#the mean slope, conservative choice
          prior(normal(0, 10), class = 'sigma')) #very vague prior 

# Draw the consequences of priors AND likelihood
m1_3_prior <- brm(m1_3, dataV1ASD, prior = prior, sample_prior = "only")

# Prior Predictive Check
pp_check(m1_3_prior, nsample=100)

```


```{r fitting model, include= FALSE}
#Plotting height and weight against one another to get an idea of how strongly they covary
plot( dataV1ASD$ADOS ~ dataV1ASD$SocialIQc )

#Fitting the model with a predictor 
m1_3 <- map(
            alist(
            ADOS ~ dnorm(mu, sigma) ,
            mu <- a + b*SocialIQc ,
            a ~ dnorm(14, 10),
            b ~ dnorm(0 , 0.5) ,
            sigma ~ dnorm(0, 10)
            ) ,
            data=dataV1ASD )

#Table of estimates
precis( m1_3)

```

A person with a nonverbal IQ 1 point higher will in average have 0.49 point lower ADOS with a standard deviation of 0.12. 89 % of the posterior probability lies between -0.68 and -0.30. The estimate of alpha indicates that a person with non verbal IQ of 0 should have an ADOS of 13.85. The estimate for sigma informs us of the width of the distribution of ADOS around the mean. Since we have centered nonverbal IQ, the intercept also means the expected value of the outcome variable i.e. ADOS, when the predictor is at its average value.


```{r plotting the model, echo = FALSE}
# plot raw data
plot( ADOS ~ SocialIQc , dataV1ASD , col=col.alpha(rangi2,0.5) )

# define sequence of VerbalIQ to compute predictions for
# these values will be on the horizontal axis
SocialIQc.seq <- seq( from=-15, to=16 , by=1 )
# use link to compute mu
# for each sample from posterior
# and for each weight in weight.seq
mu <- link( m1_2 , data=data.frame(SocialIQc=SocialIQc.seq) )
str(mu)

# summarize the distribution of mu
mu.mean <- apply( mu , 2 , mean )
mu.HPDI <- apply( mu , 2 , HPDI , prob=0.89 )

# draw MAP line
lines(SocialIQc.seq , mu.mean )
# draw HPDI region for line
shade( mu.HPDI , SocialIQc.seq)

#prediction intervals

#For every unique weight value, you sample from a gaussian distribution with the correct mean mu for that weight using the correct value of sigma sampled from the posterior distribution. This gives you a collection of simulated heights that embody the uncertainty in the posterior as well as the uncertainty in the gaussian likelihood
sim.ADOS <- sim( m1_2 , data=list(SocialIQc=SocialIQc.seq) )
str(sim.ADOS)

ADOS.PI <- apply( sim.ADOS, 2 , PI , prob=0.89 )#summarizing the simulated heights

# draw PI region for simulated heights
shade( ADOS.PI , SocialIQc.seq )
```


Comparing models
```{r}
compare(m1_1, m1_2, m1_3)
```



2. Do the different aspects of IQ account for different portions of the variance in ADOS? 
Building a model with all predictors


```{r making a model, include = FALSE}

#Defining prior
m2_1 <- bf(ADOS ~ SocialIQc + NonVerbalIQc + VerbalIQc)

# Ask which priors need to be defined
get_prior(m2_1, data = dataV1ASD, family = gaussian)#This is very weak priors, since the distributions cross zero

# Define another prior
prior = c(prior(normal(14, 10), class = "Intercept"),
          prior(normal(0, 0.5), class = 'b', coef="SocialIQc"),#the mean slope, conservative choice
          prior(normal(0, 1), class = 'b', coef="NonVerbalIQc"),#the mean slope, conservative choice,
          prior(normal(0, 1), class = 'b', coef = "VerbalIQc"),#the mean slope, conservative choice
          prior(normal(0, 10), class = 'sigma')) #very vague prior 

# Draw the consequences of priors AND likelihood
m2_1_prior <- brm(m2_1, dataV1ASD, prior = prior, sample_prior = "only")

# Prior Predictive Check
pp_check(m2_1_prior, nsample=100)

```


```{r fitting model, include= FALSE}
#Fitting the model with a predictor 
m2_1 <- map(
            alist(
            ADOS ~ dnorm(mu, sigma) ,
            mu <- a + b1*SocialIQc + b2*NonVerbalIQc + b3*VerbalIQc ,
            a ~ dnorm(14, 10),
            b1 ~ dnorm(0 , 0.5) ,
            b2 ~ dnorm(0 , 1), 
            b3 ~ dnorm(0 , 1),
            sigma ~ dnorm(0, 10)
            ) ,
            data=dataV1ASD )

#Table of estimates
precis( m2_1, cor= TRUE)


p_load(corrplot)
## corrplot 0.84 loaded
M <- cor(dataV1ASD[,10:12])
corrplot(M, method = "circle", type = c("lower"))

```



2.1. Does it make sense to have all IQ measures in the same model? First write a few lines answering the question and motivating your answer, including a discussion as to what happens when you put all of them in the same model. Then build a model following your answer. If your answer is "no", you are not free, you still have to answer: are there alternative ways of answering the question?
2.2. Build the model, assess its quality, write a few lines interpreting the results.

3. Let's now include also the TD children. Does it make sense to ask whether IQ and ADOS are related? Motivate your answer. In any case, if you wanted to build a model to answer that question, which model would you build? Run the model, assess its quality, write a few lines interpreting the results.

```{r producing priors, include = FALSE}
#filtering the data
dataV1 <- filter(data, Visit == 1)

#centering the variables to make the intercept interpretable
dataV1$NonVerbalIQc <- scale(dataV1$NonVerbalIQ,scale=FALSE)
dataV1$VerbalIQc <- scale(dataV1$VerbalIQ,scale=FALSE)
dataV1$SocialIQc <- scale(dataV1$SocialIQ,scale=FALSE)

#removing na's to get the code to run
dataV1 <- dataV1[ complete.cases(dataV1) , ]

#Defining prior
m3_1 <- bf(ADOS ~ SocialIQc + NonVerbalIQc + VerbalIQc)

# Ask which priors need to be defined
get_prior(m3_1, data = dataV1, family = gaussian)#This is very weak priors, since the distributions cross zero

# Define another prior
prior = c(prior(normal(7.43, 10), class = "Intercept"),
          prior(normal(0, 0.5), class = 'b', coef="SocialIQc"),#the mean slope, conservative choice
          prior(normal(0, 1), class = 'b', coef="NonVerbalIQc"),#the mean slope, conservative choice,
          prior(normal(0, 1), class = 'b', coef = "VerbalIQc"),#the mean slope, conservative choice
          prior(normal(0, 10), class = 'sigma')) #very vague prior 

# Draw the consequences of priors AND likelihood
m3_1_prior <- brm(m3_1, dataV1, prior = prior, sample_prior = "only")

# Prior Predictive Check
pp_check(m3_1_prior, nsample=100)
```



```{r fitting model, include= FALSE}
#fitting the model
m3 <- rethinking::map(
    alist(
        ADOS ~ dnorm(mu,sigma),
        mu <- a + b1 * VerbalIQc + b2*NonVerbalIQ + b3*SocialIQc,
        a ~ dnorm(7.43, 10),
        b1 ~ dnorm(0, 1),
        b2 ~ dnorm(0, 1),
        b3 ~ dnorm(0, .5),
        sigma ~ dnorm(0, 10)),
    data = dataV1)

precis(m3)

plot(precis(m3))

  p_load(corrplot)

## corrplot 0.84 loaded
M <- cor(dataV1[,10:12])
corrplot(M, method = "circle", type = c("lower"))



```


```{r plotting, include = FALSE}

# make plot that superimposes the MAP values for mean ADOS over the actual data
plot( ADOS ~ VerbalIQc + NonVerbalIQc + SocialIQc , data=dataV1)
abline( a=coef(m3)["a"] , b=c(coef(m3)["b1"],coef(m3)["b2"],coef(m3)["b3"] ))


# call link without specifying new data
# so it uses original data
mu <- link(m3)

#colour columns
dataV1$Colour[dataV1$ASD==1]="aquamarine4"
dataV1$Colour[dataV1$ASD==0]="coral"

# summarize samples across cases
mu.mean <- apply( mu, drop=F , 2 , mean )
mu.PI <- apply( mu, 2 , PI )
# simulate observations
# again no new data, so uses original data
ADOS.sim <- sim( m3 , n=1e4 )
ADOS.PI <- apply( ADOS.sim , 2 , PI )

plot( mu.mean ~ dataV1$ADOS , col=dataV1$Colour , ylim=range(mu.PI) ,
    xlab="Observed ADOS" , ylab="Predicted ADOS" )
abline( a=0 , b=1 , lty=2 )

for ( i in 1:nrow(dataV1) )
    lines( rep(dataV1$ADOS[i],2) , c(mu.PI[1,i],mu.PI[2,i]) ,
        col=rangi2 )

# compute residuals
ADOS.resid <- dataV1$ADOS - mu.mean
# get ordering by ADOS rate
o <- order(ADOS.resid)
# make the plot
dotchart( ADOS.resid[o] , labels=dataV1$Loc[o] , xlim=c(-6,5) , cex=0.6 )
abline( v=0 , col=col.alpha("black",0.2) )
for ( i in 1:nrow(dataV1) ) {
    j <- o[i] # which State in order
    lines( dataV1$ADOS[j]-c(mu.PI[1,j],mu.PI[2,j]) , rep(i,2) )
    points( dataV1$ADOS[j]-c(ADOS.PI[1,j],ADOS.PI[2,j]) , rep(i,2),
            pch=3 , cex=0.6 , col="gray" )
}
mtext("Average prediction error by subject with 89% interval of the mean (black line) and 89% prediction interval (grey +)")





```


4. Let's discuss contents:
4.1. You have three scores for IQ, do they show shared variance? Is that the same in TD and ASD? What does that tell us about IQ?
4.2. You have explored the relation between IQ and ADOS. How do you explain that relation from a cognitive perspective? N.B. You can present alternative hypotheses.

5. Bonus questions: Including measurement errors. 
5.1. Let's assume that ADOS has a measurement error of 1. How would you model that in any of the previous models? 
5.2. We know that IQ has an estimated measurement error of 2.12. How would you include that? 


